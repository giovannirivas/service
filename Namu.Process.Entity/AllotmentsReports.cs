﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Namu.Process.Entity
{
    public class AllotmentsReports
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string EmailTemplate { get; set; }
        public int DaysToProcess { get; set; }
        public string Email { get; set; }
        public DateTime LastRun { get; set; }

    }
}
