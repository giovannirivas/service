﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Namu.Process.Entity
{
    public class ProductAllotment
    {
        public string RoomTypeName { get; set; }
        public string RatePlanName { get; set; }
        public DateTime Date { get; set; }
        public string Restrictions { get; set; }
        public int Allotment { get; set; }

    }

    public class AllotmentGroup
    {
        public string RoomTypeName { get; set; }
        public string RatePlanName { get; set; }
        public List<ProductAllotment> Data { get; set; }

    }

}
