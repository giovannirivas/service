﻿using Namu.Process;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var processAllotment = new ProcessAllotmentsData(ConfigurationManager.ConnectionStrings["connectionString"].ToString());
            ProcessMail emailProcess = new ProcessMail(ConfigurationManager.AppSettings["AWSAccessKey"],
                ConfigurationManager.AppSettings["AWSSecretKey"], "");

            var reports = processAllotment.GetAllotmentsToProcess();
            foreach (var report in reports)
            {
                
                var productAllotment = processAllotment.GetProductAllotments(report.ProductId,report.DaysToProcess);
                var htmlTemplate = processAllotment.BuildReportHTML(report.EmailTemplate, productAllotment);
                emailProcess.SendMail(htmlTemplate, report.Email);

            }
        }
    }
}
